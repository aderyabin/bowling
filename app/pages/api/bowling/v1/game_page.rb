class Api::Bowling::V1::GamePage < Tram::Page
  param :game

  section :id
  section :frames

  delegate :id, to: :game

  delegate :frames, to: :game
end
