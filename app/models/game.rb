require_relative 'game/frames_type'

class Game < ApplicationRecord
  attribute :frames, FramesType.new
end
