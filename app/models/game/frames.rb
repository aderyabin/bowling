# frozen_string_literal: true

require_relative 'frame'

class Game < ApplicationRecord
  class Frames
    extend Forwardable

    def_delegators :@collection, *[].public_methods

    def initialize(array_or_hash = [])
      @collection = case array_or_hash
                    when Hash
                      [Game::Frame.new(array_or_hash)]
                    else
                      Array(array_or_hash).map do |frame|
                        frame.is_a?(Game::Frame) ? frame : Game::Frame.new(frame)
                      end
                    end
    end

    def to_a
      @collection
    end

    def to_h
      to_a.map(&:as_json)
    end
  end
end
