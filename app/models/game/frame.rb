class Game < ApplicationRecord
  class Frame
    attr_reader :pins
    attr_reader :score
    attr_reader :id

    delegate :count, :sum, to: :@pins

    def initialize(attributes = {})
      attributes.symbolize_keys!
      self.id = attributes[:id]
      self.pins = attributes[:pins] || []
      self.score = attributes[:score]
    end

    attr_writer :id

    attr_writer :pins

    attr_writer :score

    def as_json(_attrs = {})
      {
        id: id,
        pins: pins,
        score: score
      }
    end

    def ==(other)
      as_json == other.as_json
    end

    def strike?
      pins.first&.to_i == 10
    end

    def spare?
      !strike? && count >= 2 && pins[0..1].sum == 10
    end

    def final?
      id == 10
    end
  end
end
