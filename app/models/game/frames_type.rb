# frozen_string_literal: true

require_relative 'frames'

class Game < ApplicationRecord
  class FramesType < ActiveRecord::Type::Value
    def type
      :jsonb
    end

    def cast(value)
      Game::Frames.new(value)
    end

    def deserialize(value)
      if value.is_a?(String)
        decoded = begin
                    ::ActiveSupport::JSON.decode(value)
                  rescue StandardError
                    nil
                  end

        Game::Frames.new(decoded)

      else
        super
      end
    end

    def changed_in_place?(_raw_old_value, _new_value)
      true
    end

    def serialize(value)
      case value
      when Array, Hash, Game::Frames
        ::ActiveSupport::JSON.encode(value)
      else
        super
      end
    end
  end
end
