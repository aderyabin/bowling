class ApplicationRecord < ActiveRecord::Base
  connects_to database: { writing: :primary, reading: :primary }
  self.abstract_class = true
end
