class Games::CheckFrameFinished < BaseServiceObject
  param :frame

  def call
    return false unless frame
    return frame.count == 2 || frame.sum == 10 unless frame.final?

    frame.count == if frame.sum >= 10
                     3
                   else
                     2
                   end
  end
end
