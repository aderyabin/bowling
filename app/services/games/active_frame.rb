class Games::ActiveFrame < BaseServiceObject
  param :game

  def call
    return nil if Games::CheckFinished.call(game)

    @frames_count = game.frames.count
    return new_frame if @frames_count.zero?

    last_frame = game.frames.last
    return last_frame unless Games::CheckFrameFinished.call(last_frame)

    new_frame
  end

  private

  def new_frame
    frame = Game::Frame.new(id: @frames_count + 1)
    game.frames.push(frame)
    frame
  end
end
