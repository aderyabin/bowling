class Games::Rescore < BaseServiceObject
  param :game

  def call
    @total_score = game.frames.map(&:score).compact.last.to_i
    game.frames.each do |frame|
      score(frame) if Games::CheckFrameFinished.call(frame) && frame.score.nil?
    end

    game
  end

  private

  def score(frame) # rubocop:disable Metrics/AbcSize
    if frame.final? || (!frame.strike? && !frame.spare?)
      @total_score += frame.sum
      frame.score = @total_score
    else
      pins = game.frames[frame.id..-1].map(&:pins).flatten
      if frame.spare? && pins[0]
        @total_score += 10 + pins[0]
        frame.score = @total_score
      elsif frame.strike? && pins[0] && pins[1]
        @total_score += 10 + pins[0] + pins[1]
        frame.score = @total_score
      end
    end
  end
end
