class Games::CheckFinished < BaseServiceObject
  param :game

  def call
    frame = game.frames.last
    return false unless frame

    frame.final? && Games::CheckFrameFinished.call(frame)
  end
end
