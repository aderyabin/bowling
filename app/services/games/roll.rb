class Games::Roll < BaseServiceObject
  param :game
  param :count

  def call
    return false unless valid_count?
    return false if Games::CheckFinished.call(game)

    @frame = Games::ActiveFrame.call(game)
    @frame.pins.push(count)
    Games::Rescore.call(game)
    valid? && game.save
  end

  private

  def valid?
    return valid_normal_frame? unless @frame.final?

    valid_final_frame?
  end

  def valid_normal_frame?
    @frame.sum <= 10 && @frame.count <= 2
  end

  def valid_final_frame?
    if @frame.strike?
      return false if @frame.count > 3
      return true if @frame.pins[1].to_i == 10

      @frame.pins[1..2].sum <= 10
    elsif @frame.spare?
      @frame.count <= 3
    else
      valid_normal_frame?
    end
  end

  def valid_count?
    return false unless count.is_a?(Integer)
    return false if count.negative?

    true
  end
end
