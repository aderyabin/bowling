class ApiController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  include ActionController::MimeResponds

  rescue_from StandardError do |exception|
    case exception
    when ActiveRecord::RecordNotFound
      respond_to do |format|
        format.json do
          render json: { error: 'Resource not found' }, status: :not_found
        end

        format.html do
          raise exception
        end
      end
    when ActiveRecord::RecordInvalid
      respond_to do |format|
        format.json do
          render json: { error: exception.message }, status: :bad_request
        end

        format.html do
          raise exception
        end
      end
    else
      respond_to do |format|
        format.json do
          render json: {
            error: exception.message.to_s,
            error_details: exception.backtrace.join("\n").to_s
          }, status: :internal_server_error
        end

        format.html do
          raise exception
        end
      end
    end
  end
end
