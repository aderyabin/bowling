module Api
  module Bowling
    module V1
      class GamesController < ApiController
        def create
          game = Game.create
          render json: GamePage.new(game).to_h, status: :created
        end

        def roll
          if Games::Roll.call(game, params[:count])
            render json: GamePage.new(game).to_h
          else
            head :unprocessable_entity
          end
        end

        def show
          render json: GamePage.new(game).to_h
        end

        private

        def game
          @game ||= Game.find(params[:id])
        end
      end
    end
  end
end
