FactoryBot.define do
  factory :game do
    trait :sample do
      frames do
        [
          { id: 1, score: 9, pins: [4, 5] },
          { id: 2, score: nil, pins: [3] }
        ]
      end
    end

    trait :perfect do
      frames do
        [
          { id: 1, score: 30, pins: [10] },
          { id: 2, score: 60, pins: [10] },
          { id: 3, score: 90, pins: [10] },
          { id: 4, score: 120, pins: [10] },
          { id: 5, score: 150, pins: [10] },
          { id: 6, score: 180, pins: [10] },
          { id: 7, score: 210, pins: [10] },
          { id: 8, score: 240, pins: [10] },
          { id: 9, score: 270, pins: [10] },
          { id: 10, score: 300, pins: [10, 10, 10] }
        ]
      end
    end
  end
end
