require 'rails_helper'

RSpec.describe Game, type: :model do
  describe '#frames' do
    let(:game) { build :game, frames: [Game::Frame.new] }

    it 'assigns frames' do
      expect(game.frames).to eq([Game::Frame.new])
    end
  end
end
