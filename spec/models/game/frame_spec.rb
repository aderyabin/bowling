require 'rails_helper'

RSpec.describe Game::Frame do
  describe '#strike?' do
    context 'when first roll knoks ten' do
      it 'returns true' do
        expect(described_class.new(pins: [10])).to be_strike
      end
    end

    context 'when second roll knoks ten' do
      it 'returns false' do
        expect(described_class.new(pins: [0, 10])).not_to be_strike
      end
    end
  end

  describe '#spare?' do
    context 'when first roll knoks ten' do
      it 'returns false' do
        expect(described_class.new(pins: [10])).not_to be_spare
      end
    end

    context 'when second roll knoks ten' do
      it 'returns true' do
        expect(described_class.new(pins: [0, 10])).to be_spare
      end
    end

    context 'when both rolls knoks ten' do
      it 'returns true' do
        expect(described_class.new(pins: [3, 7])).to be_spare
      end
    end
  end

  describe '#final?' do
    context 'when id ==1 ' do
      it 'returns false' do
        expect(described_class.new(id: 1)).not_to be_final
      end
    end

    context 'when id == 10' do
      it 'returns true' do
        expect(described_class.new(id: 10)).to be_final
      end
    end
  end
end
