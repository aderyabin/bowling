require 'rails_helper'

describe Games::Roll do
  subject { described_class.call(game, count) }

  let(:game) { create :game }

  context 'game finished' do
    let(:count) { 3 }

    before do
      allow(Games::CheckFinished).to receive(:call).and_return(true)
    end

    it { is_expected.to be_falsey }
  end

  context 'invalid count' do
    context 'greater than pins count' do
      let(:count) { 12 }

      it { is_expected.to be_falsy }
    end

    context 'with negative count' do
      let(:count) { -1 }

      it { is_expected.to be_falsy }
    end

    context 'with string value' do
      let(:count) { '1' }

      it { is_expected.to be_falsy }
    end
  end

  context 'zero pins' do
    let(:count) { 0 }

    it { is_expected.to be_truthy }
  end

  context 'with valid count' do
    let(:count) { 3 }

    it { is_expected.to be_truthy }

    it 'saves roll' do
      expect  do
        subject
      end.to change { game.frames.count }.by(1)
    end

    it 'scores frame' do
      described_class.call(game, count)
      described_class.call(game, count)
      expect(game.reload.frames[0].score).to eq(6)
    end
  end

  context 'perfect game' do
    before do
      13.times { described_class.call game, 10 }
    end

    it 'finish game' do
      expect(Games::CheckFinished.call(game)).to eq(true)
    end

    it 'scores 300' do
      expect(game.frames.last.score).to eq(300)
    end
  end

  context 'perfect game' do
    before do
      13.times { described_class.call game, 10 }
    end

    it 'finish game' do
      expect(Games::CheckFinished.call(game)).to eq(true)
    end

    it 'saves ten frames' do
      expect(game.frames.count).to eq(10)
    end

    it 'scores 300' do
      expect(game.frames.last.score).to eq(300)
    end
  end

  context 'spare game' do
    before do
      23.times { described_class.call game, 5 }
    end

    it 'finish game' do
      expect(Games::CheckFinished.call(game)).to eq(true)
    end

    it 'saves ten frames' do
      expect(game.frames.count).to eq(10)
    end

    it 'scores 150' do
      expect(game.frames.last.score).to eq(150)
    end
  end

  context 'bad game' do
    before do
      20.times { described_class.call game, 2 }
    end

    it 'finish game' do
      expect(Games::CheckFinished.call(game)).to eq(true)
    end

    it 'saves ten frames' do
      expect(game.frames.count).to eq(10)
    end

    it 'scores 40' do
      expect(game.frames.last.score).to eq(40)
    end
  end
end
