require 'rails_helper'

describe Games::CheckFrameFinished do
  subject { described_class.call(frame) }

  context 'no frame' do
    let(:frame) { nil }

    it { is_expected.to be_falsey }
  end

  context 'not final frame' do
    context 'strike' do
      let(:frame) { Game::Frame.new(id: 5, pins: [10]) }

      it { is_expected.to be_truthy }
    end

    context 'first roll' do
      let(:frame) { Game::Frame.new(id: 5, pins: [6]) }

      it { is_expected.to be_falsey }
    end

    context 'second roll' do
      let(:frame) { Game::Frame.new(id: 5, pins: [6, 3]) }

      it { is_expected.to be_truthy }
    end
  end

  context 'final frame' do
    context 'strike' do
      context 'with one roll' do
        let(:frame) { Game::Frame.new(id: 10, pins: [10]) }

        it { is_expected.to be_falsey }
      end

      context 'with two rolls' do
        context 'with strike' do
          let(:frame) { Game::Frame.new(id: 10, pins: [10, 10]) }

          it { is_expected.to be_falsey }
        end
      end

      context 'with three rolls' do
        context 'with strike' do
          let(:frame) { Game::Frame.new(id: 10, pins: [10, 10, 10]) }

          it { is_expected.to be_truthy }
        end
      end
    end

    context 'spare' do
      context 'with two rolls' do
        let(:frame) { Game::Frame.new(id: 10, pins: [0, 10]) }

        it { is_expected.to be_falsey }
      end

      context 'with three rolls' do
        context 'when last rolls all' do
          let(:frame) { Game::Frame.new(id: 10, pins: [0, 10, 10]) }

          it { is_expected.to be_truthy }
        end

        context 'when last rolls not all' do
          let(:frame) { Game::Frame.new(id: 10, pins: [0, 10, 6]) }

          it { is_expected.to be_truthy }
        end
      end
    end

    context 'normal' do
      context 'first roll' do
        let(:frame) { Game::Frame.new(id: 5, pins: [6]) }

        it { is_expected.to be_falsey }
      end

      context 'second roll' do
        let(:frame) { Game::Frame.new(id: 5, pins: [6, 3]) }

        it { is_expected.to be_truthy }
      end
    end
  end
end
