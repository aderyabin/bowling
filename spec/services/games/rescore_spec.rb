require 'rails_helper'

describe Games::Rescore do
  subject { described_class.call(game) }

  let(:game) { create :game, frames: frames }

  context 'without frames' do
    let(:frames) { [] }

    it 'returns game' do
      expect do
        subject
      end.not_to change(game, :reload)
    end
  end

  context 'first frame' do
    context 'with strike' do
      context 'without more rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [10], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with one more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [10], score: nil),
            Game::Frame.new(id: 1, pins: [1], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with two more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [10], score: nil),
            Game::Frame.new(id: 1, pins: [10], score: nil),
            Game::Frame.new(id: 1, pins: [2], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(22)
        end
      end
    end

    context 'with spare' do
      context 'without more rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [5, 5], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with one more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [5, 5], score: nil),
            Game::Frame.new(id: 2, pins: [8], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(18)
        end
      end
    end

    context 'with not downed pins' do
      context 'one roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [5], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'two rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [0, 8], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(8)
        end
      end
    end
  end

  context 'second frame' do
    let(:frames) do
      [
        Game::Frame.new(id: 1, pins: [5, 0], score: 5),
        Game::Frame.new(id: 2, pins: [5, 3], score: nil)
      ]
    end

    it 'scores as sum with previous' do
      expect(subject.frames[1].score).to eq(13)
    end
  end

  context 'final frame' do
    context 'with strike' do
      context 'without more rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [10], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with one more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [10, 4], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with two more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [10, 10, 3], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(23)
        end
      end
    end

    context 'with spare' do
      context 'without more rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 1, pins: [5, 5], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'with one more roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [5, 5, 9], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(19)
        end
      end
    end

    context 'with not downed pins' do
      context 'one roll' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [5], score: nil)
          ]
        end

        it 'not scores' do
          expect(subject.frames[0].score).to be_nil
        end
      end

      context 'two rolls' do
        let(:frames) do
          [
            Game::Frame.new(id: 10, pins: [1, 4], score: nil)
          ]
        end

        it 'scores correctly' do
          expect(subject.frames[0].score).to eq(5)
        end
      end
    end
  end
end
