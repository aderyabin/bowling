require 'rails_helper'

describe Games::ActiveFrame do
  subject { described_class.call(game) }

  let(:game) { build :game, frames: frames }

  context 'no frames' do
    let(:frames) { [] }

    it 'adds new frame' do
      expect do
        subject
      end.to change { game.frames.count }.by(1)
    end

    it 'returns a first frame' do
      expect(subject.to_json).to eq(Game::Frame.new(id: 1).to_json)
    end
  end

  context 'game finished' do
    let(:frames) { [] }

    before do
      allow(Games::CheckFinished)
        .to receive(:call)
        .with(game)
        .and_return(true)
    end

    it 'does not change frames' do
      expect do
        subject
      end.not_to change(game, :frames)
    end

    it { is_expected.to be_nil }
  end

  context 'last frame not final' do
    let(:frames) { [Game::Frame.new(id: 1), frame] }
    let(:frame) { Game::Frame.new(id: 2) }

    before do
      allow(frame)
        .to receive(:final?)
        .and_return(false)
    end

    context 'and finished' do
      before do
        allow(Games::CheckFrameFinished)
          .to receive(:call)
          .and_return(true)
      end

      it 'add new frames' do
        expect do
          subject
        end.to change { game.frames.count }.by(1)
      end

      it 'returns new frame' do
        expect(subject).to eq(Game::Frame.new(id: 3))
      end
    end

    context 'and not finished' do
      before do
        allow(Games::CheckFrameFinished)
          .to receive(:call)
          .and_return(false)
      end

      it 'does not change frames' do
        expect do
          subject
        end.not_to change(game, :frames)
      end

      it 'returns a frame' do
        expect(subject).to eq(frame)
      end
    end
  end

  context 'last frame is final' do
    let(:frames) { [Game::Frame.new(id: 1), frame] }
    let(:frame) { Game::Frame.new(id: 2) }

    before do
      allow(frame)
        .to receive(:final?)
        .and_return(true)
    end

    context 'and not finished' do
      before do
        allow(Games::CheckFrameFinished)
          .to receive(:call)
          .and_return(false)
      end

      it 'does not change frames' do
        expect do
          subject
        end.not_to change(game, :frames)
      end

      it 'returns a frame' do
        expect(subject).to eq(frame)
      end
    end
  end
end
