require 'rails_helper'

describe Games::CheckFinished do
  subject { described_class.call(game) }

  let(:game) { build :game, frames: frames }

  context 'without frames' do
    let(:frames) { [] }

    it { is_expected.to be_falsey }
  end

  context 'with frames' do
    let(:frame) { Game::Frame.new }
    let(:frames) { [frame.as_json] }

    context 'when last frame finished but not final' do
      before do
        allow(frame).to receive(:final?).and_return(false)
        allow(Games::CheckFrameFinished).to receive(:call).with(frame).and_return(false)
      end

      it { is_expected.to be_falsey }
    end

    context 'when last frame not finished but final' do
      before do
        allow(frame).to receive(:final?).and_return(true)
        allow(Games::CheckFrameFinished).to receive(:call).with(frame).and_return(false)
      end

      it { is_expected.to be_falsey }
    end

    context 'when last frame finished and final' do
      before do
        allow(frame).to receive(:final?).and_return(true)
        allow(Games::CheckFrameFinished).to receive(:call).with(frame).and_return(true)
      end

      it { is_expected.to be_falsey }
    end
  end
end
