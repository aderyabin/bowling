require 'rails_helper'

RSpec.describe 'Bowling API v1', type: :request do
  let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }

  context 'POST /api/bowling/v1/games' do
    subject { post '/api/bowling/v1/games', headers: headers }

    it 'returns 201' do
      subject
      expect(response).to have_http_status(201)
    end

    it 'created new game' do
      expect  do
        subject
      end.to change(Game, :count).by(1)
    end

    it 'returns empty game' do
      subject
      expect(json_response).to eq('id' => Game.last.id, 'frames' => [])
    end
  end

  context 'GET /api/bowling/v1/games/:id' do
    subject { get "/api/bowling/v1/games/#{game_id}", headers: headers }

    context 'with invalid id' do
      let(:game_id) { Game.last&.id.to_i + 1 }

      it 'returns 404' do
        subject
        expect(response).to have_http_status(404)
      end

      it 'returns error body' do
        subject
        expect(json_response).to eq('error' => 'Resource not found')
      end
    end

    context 'with valid id' do
      let(:game) { create :game, :sample }
      let(:game_id) { game.id }

      it 'returns 200' do
        subject
        expect(response).to have_http_status(200)
      end

      it 'returns error body' do
        subject
        expect(json_response).to eq({
          'id' => game_id,
          'frames' => [
            { 'id' => 1, 'score' => 9, 'pins' => [4, 5] },
            { 'id' => 2, 'score' => nil, 'pins' => [3] }
          ]
        }.stringify_keys)
      end
    end
  end

  context 'POST /api/bowling/v1/games/:id/roll' do
    subject { post "/api/bowling/v1/games/#{game_id}/roll", params: params.to_json, headers: headers }

    context 'with invalid id' do
      let(:game_id) { Game.last&.id.to_i + 1 }
      let(:params) { {} }

      it 'returns 404' do
        subject
        expect(response).to have_http_status(404)
      end

      it 'returns error body' do
        subject
        expect(json_response).to eq('error' => 'Resource not found')
      end
    end

    context 'with valid id' do
      let(:game) { create :game }
      let(:game_id) { game.id }

      context 'with invalid params' do
        context 'with empty params' do
          let(:params) { {} }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end

        context 'with negative value' do
          let(:params) { { count: -2 } }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end

        context 'with string value' do
          let(:params) { { count: 'sfsdfs' } }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end

        context 'with greater 10' do
          let(:params) { { count: 12 } }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end

        context 'with finished game' do
          let(:game) { create :game, :perfect }
          let(:game_id) { game.id }
          let(:params) { { count: 1 } }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end

        context 'with pins greater than frame' do
          let(:game) { create :game, :sample }
          let(:game_id) { game.id }
          let(:params) { { count: 9 } }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end
      end

      context 'with valid params' do
        let(:game) { create :game }
        let(:game_id) { game.id }
        let(:params) { { count: 4 } }

        it 'calls roll service' do
          expect(Games::Roll).to receive(:call).with(game, 4)
          subject
        end

        it 'returns 200' do
          subject
          expect(response).to have_http_status(200)
        end

        it 'saves roll in game' do
          expect  do
            subject
          end.to change { JSON.parse game.reload.frames.to_json }
            .from([])
            .to([{ 'id' => 1, 'pins' => [4], 'score' => nil }])
        end

        it 'returns valid json' do
          subject
          expect(json_response).to eq(
            'frames' => [
              { 'id' => 1, 'pins' => [4], 'score' => nil }
            ],
            'id' => game.id
          )
        end

        context 'with invalid headers' do
          let(:headers) { {} }

          it 'returns 422' do
            subject
            expect(response).to have_http_status(422)
          end
        end
      end
    end
  end
end
