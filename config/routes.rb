Rails.application.routes.draw do
  namespace :api, constraints: { format: :json }, defaults: { format: 'json' } do
    namespace :bowling do
      namespace :v1 do
        resources :games, only: %i[create show] do
          post :roll, on: :member
        end
      end
    end
  end
end
