# BOWLING GAME

## Start a dev server

1. Copy `.env.example` to `.env.development`
2. Set `DATABASE_URL`
3. Run `bundle install`
4. Run `bin/rails db:setup`
5. Run server `bin/rails s`

## API

1. Create a game

```
curl -X POST \
     -H "Content-Type: application/json" \
     http://localhost:3000/api/bowling/v1/games/
```

2. Roll a ball

```
curl -X POST \
     -d '{"count": 10}' \
     -H "Content-Type: application/json" \
     http://localhost:3000/api/bowling/v1/games/:game_id/roll
```

3. Game info

```
curl http://localhost:3000/api/bowling/v1/games/:game_id
```

## Testing

1. Copy `.env.example` to `.env.test`
2. Set `DATABASE_URL`
3. Run `RAILS_ENV=test bin/rails db:setup`
4. Run specs `bundle exec rspec`


## Linter

1. Run `bundle exec rubocop`
